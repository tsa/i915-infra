<% from common import htmlname, ttip, htmlttip, boxedletter %>

<%inherit file="common.mako"/>
<%namespace name="helpers" file="helpers.mako"/>

<%block name="title">${host} CI History</%block>

<table>
  <tr>
    <th></th>
    % for build in builds:
      <th><span class="v">
	<a href="${path}${build}/filelist.html">${build}</a>
      </th>
    % endfor
  </tr>
  % for test in tests:
    <tr id="${test}">
      <td class="h"><a href="${htmlname(test)}">${test}</a></td>
      % for build in builds:
	<%
	  try: hostname = jsons[(build, host)]['tests'][test]['hostname']
	  except: hostname = host
	  try:
	      node = jsons[(build, host)]['tests'][test]
	      res,tooltip = ttip(node)
	      letter = boxedletter(node)
	      link = path+build+"/"+hostname+"/"+htmlname(test)
	  except:
	      res,tooltip = ('notrun', "")
	      letter = '&#8199;'
	      link = ""

	  tooltip=htmlttip(tooltip)
	%>
	${helpers.result_cell(res, link, letter, test, build, tooltip)}
      % endfor
  </tr>
  % endfor
</table>
